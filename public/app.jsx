var React = require('react');
var ReactDOM = require('react-dom');

var Greeter = require('./components/Greeter');

var firstMessage = 'This is the 1st message';

ReactDOM.render(
  <Greeter name='JamesR' message={firstMessage} />,
  document.getElementById('app')
);
